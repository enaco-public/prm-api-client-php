# Enaco PRM SDK for PHP

## Installing

### Using Composer

Add the following to you project's composer.json

```javascript
{
    "require": {
        "lig/prm-api-client-php": "~1.0"
    },
    "repositories": [
        {
            "type": "composer",
            "url": "https://gittrack.enaco.fr/public-packages"
        }
    ]
}
```

next run ``composer update lig/prm-api-client-php --prefer-dist``


### Using Zip

Unzip the prm-api-client-php.zip file in your libs directory.


## Usage

```php
<?php

// require composer autoloader
require_once '/path/to/vendor/autoload.php';
// or library autoloader
require_once '/path/to/library/prm-api-autoloader.php';

$client = \LIG\PRM\API\Client::factory(array(
    'base_url' => 'URL',
    'api_key' => 'YOUR_API_KEY'
));

// Get formations
/** @var \Guzzle\Http\Message\Response $res **/
$res = $client->getFormations();

if($res->isError()){
    /** var array $errors **/
    $errors = $client->getErrors($res);
}else {
    echo $res->getStatusCode() . ' ' . $res->getReasonPhrase(); // 200 OK
    
    /** @var array $content **/
    $content = $res->json();
}


// Add coupon
/** @var \Guzzle\Http\Message\Response $res **/
$res = $client->addCoupon(
    $email,
    $formation,
    $firstname = null,
    $lastname = null,
    $idGender = null,
    $birthday = null,
    $phone = null,
    $address1 = null,
    $address2 = null,
    $postcode = null,
    $city = null,
    $country = null,
    $type = null
);

if($res->isError()){
    /** var array $errors **/
    $errors = $client->getErrors($res);
}else {
    
    echo $res->getStatusCode() . ' ' . $res->getReasonPhrase(); // 201 Created
    
    /** @var array $content **/
    $content = $res->json();
    
}

```