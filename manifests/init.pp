# Path d'exécution par default de Exec
Exec {
path => ["/usr/bin", "/bin", "/usr/sbin", "/sbin", "/usr/local/bin", "/usr/local/sbin"]
}

package { 'zip':
    ensure => "installed"
}
