<?php

return array(
    "name" => "PRM API",
    "apiVersion" => "v1",
    "operations" => array(
        "getProspectByPhone" => array(
            "class" => "LIG\\PRM\\API\\Command\\GetProspectByPhoneCommand",
            "responseClass" => "Prospect", // LIG\\PRM\\API\\Model\\Prospect
            "summary" => "Get a prospect by phone",
        ),
        "addCoupon" => array(
            "class" => "LIG\\PRM\\API\\Command\\AddCouponCommand",
            "responseClass" => "Coupon",
            "summary" => "Create a coupon",
        ),
        "newProspect" => array(
            "httpMethod" => "GET",
            "uri" => "customers/new",
            "summary" => "get a new prospect",
            "responseClass" => "newProspect"
        ),
        "getProspectByEmail" => array(
            "httpMethod" => "GET",
            "uri" => "customers/show",
            "summary" => "Get a prospect by email",
            "parameters" => array(
                "criteria[email]" => array(
                    "type" => "string",
                    "location" => "query"
                )
            )
        ),
        "getCountries" => array(
            "httpMethod" => "GET",
            "uri" => "countries",
            "summary" => "Get a list of countries",
            "responseClass" => "Country",
            "parameters" => array(
                "limit" => array(
                    "location" => "query",
                    "type" => "integer",
                    "required" => false
                )
            )
        ),
        "getLevelsStudy" => array(
            "httpMethod" => "GET",
            "uri" => "levelsstudy",
            "summary" => "Get a list of levels studys",
            "responseClass" => "LevelStudy",
            "parameters" => array(
                "limit" => array(
                    "location" => "query",
                    "type" => "integer",
                    "required" => false
                )
            )
        ),
        "getProspect" => array(
            "httpMethod" => "GET",
            "uri" => "customers/{id}",
            "summary" => "Get a prospect by Id",
            "responseClass" => "Prospect", //LIG\\PRM\\API\\Model\\Prospect
            "parameters" => array(
                "id" => array(
                    "location" => "uri",
                    "description" => "Id",
                    "required" => true
                )
            )
        ),
        "addProspect" => array(
            "httpMethod" => "POST",
            "uri" => "customers",
            "summary" => "Create a prospect",
            "responseClass" => "AddProspectOutput",
            "parameters" => array(
                "customer[email]" => array(
                    "location" => "postField",
                ),
                "customer[firstname]" => array(
                    "location" => "postField",
                ),
                "customer[lastname]" => array(
                    "location" => "postField",
                ),
                "customer[idGender]" => array(
                    "location" => "postField",
                ),
                "customer[birthday]" => array(
                    "location" => "postField",
                ),
                "customer[country]" => array(
                    "location" => "postField",
                )
            )
        ),
        "patchProspect" => array(
            "httpMethod" => "PATCH",
            "uri" => "customers/{id}",
            "summary" => "Create a prospect",
            "responseClass" => "AddProspectOutput",
            "parameters" => array(
                "id" => array(
                    "location" => "uri",
                    "description" => "Id",
                    "required" => true
                ),
                "customer[email]" => array(
                    "location" => "postField",
                ),
                "customer[firstname]" => array(
                    "location" => "postField",
                ),
                "customer[lastname]" => array(
                    "location" => "postField",
                ),
                "customer[idGender]" => array(
                    "location" => "postField",
                ),
                "customer[birthday]" => array(
                    "location" => "postField",
                ),
                "customer[country]" => array(
                    "location" => "postField",
                )
            )
        ),
        "addProspectCoupon" => array(
            "httpMethod" => "POST",
            "uri" => "coupons",
            "summary" => "Create a coupon to a existing prospect",
            "responseClass" => "Prospect",
            "parameters" => array(
                "coupon[type]" => array(
                    "location" => "postField",
                ),
                "coupon[customer]" => array(
                    "location" => "postField",
                    "required" => true,
                ),
                "coupon[formation]" => array(
                    "location" => "postField",
                ),
          		"coupon[accepterVisio]" => array(
            		"location" => "postField",
           		)
            )
        ),
        "addAddress" => array(
            "httpMethod" => "POST",
            "uri" => "addresses",
            "summary" => "Create an address",
            "responseClass" => "Address",
            "parameters" => array(
                "address[customer]" => array(
                    "location" => "postField",
                    "required" => true,
                ),
                "address[address1]" => array(
                    "location" => "postField",
                ),
                "address[address2]" => array(
                    "location" => "postField",
                ),
                "address[postcode]" => array(
                    "location" => "postField",
                ),
                "address[city]" => array(
                    "location" => "postField",
                ),
                "address[phone]" => array(
                    "location" => "postField",
                ),
                "address[idCountry]" => array(
                    "location" => "postField",
                )
            )
        ),
        "patchAddress" => array(
            "httpMethod" => "PATCH",
            "uri" => "addresses/{id}",
            "summary" => "Patch an address",
            "responseClass" => "Address",
            "parameters" => array(
                "id" => array(
                    "location" => "uri",
                    "description" => "Id",
                    "required" => true
                ),
                "address[customer]" => array(
                    "location" => "postField",
                    "required" => true,
                ),
                "address[address1]" => array(
                    "location" => "postField",
                ),
                "address[address2]" => array(
                    "location" => "postField",
                ),
                "address[postcode]" => array(
                    "location" => "postField",
                ),
                "address[city]" => array(
                    "location" => "postField",
                ),
                "address[phone]" => array(
                    "location" => "postField",
                ),
                "address[idCountry]" => array(
                    "location" => "postField",
                )
            )
        ),
        "getAddressByPhone" => array(
            "httpMethod" => "GET",
            "uri" => "addresses/show",
            "summary" => "Retrieves an address by phone",
            "responseClass" => "Address",
            "parameters" => array(
                "criteria[phone]" => array(
                    "type" => "string",
                    "location" => "query"
                )
            )
        ),
        "getAddressByPhoneMobile" => array(
            "httpMethod" => "GET",
            "uri" => "addresses/show",
            "summary" => "Retrieves an address by phoneMobile",
            "responseClass" => "Address",
            "parameters" => array(
                "criteria[phoneMobile]" => array(
                    "type" => "string",
                    "location" => "query"
                )
            )
        ),
        "getAddressByProspect" => array(
            "httpMethod" => "GET",
            "uri" => "addresses/show",
            "summary" => "Retrieves an address by prospect id",
            "responseClass" => "Address",
            "parameters" => array(
                "criteria[customer]" => array(
                    "type" => "string",
                    "location" => "query"
                )
            )
        ),
        "getFormations" => array(
            "httpMethod" => "GET",
            "uri" => "categories",
            "summary" => "Retrieves all formations",
            "responseClass" => "Formation",
            "parameters" => array(
                "lang" => array(
                    "type" => "string",
                    "location" => "query"
                ),
                "criteria[activeDoc]" => array(
                    "type" => "string",
                    "location" => "query",
                    "default" => 1
                )
            )
        ),
        "getGenders" => array(
            "httpMethod" => "GET",
            "uri" => "genders",
            "summary" => "Retrieves all genders",
            "responseClass" => "Gender",
            "parameters" => array(
                "lang" => array(
                    "type" => "string",
                    "location" => "query"
                ),
                "criteria[activeDoc]" => array(
                    "type" => "string",
                    "location" => "query",
                    "default" => 1
                )
            )
        ),
        "getSituations" => array(
            "httpMethod" => "GET",
            "uri" => "situations",
            "summary" => "Retrieves all situations",
            "responseClass" => "Situation",
            "parameters" => array(
                "lang" => array(
                    "type" => "string",
                    "location" => "query"
                ),
                "criteria[activeDoc]" => array(
                    "type" => "string",
                    "location" => "query",
                    "default" => 1
                )
            )
        )
    ),
    "models" => array(
		"Situation" => array(
    		"type" => "object",
    		"additionalProperties" => array(
    			"location" => "json"
    		)
    	),
        "Formation" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "Gender" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "Prospect" => array(
            "type" => "object",
            "properties" => array(
                "status" => array(
                    "location" => "statusCode",
                    "type" => "integer"
                )
            ),
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "newProspect" => array(
            "type" => "object",
            "properties" => array(
                "children" => array(
                    "location" => "json"
                )
            ),
            "additionalProperties" => array(
                "children" => array(
                    "location" => "json"
                )
            )
        ),
        "Address" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "Country" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "LevelStudy" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "Coupon" => array(
            "type" => "object",
            "additionalProperties" => array(
                "location" => "json"
            )
        ),
        "AddProspectOutput" => array(
            "type" => "object",
            "properties" => array(
                "status" => array(
                    "location" => "statusCode",
                    "type" => "integer"
                ),
                "location" => array(
                    "location" => "header",
                    "sentAs" => "Location",
                    "type" => "string"
                )
            ),
            "additionalProperties" => array(
                "location" => "json"
            )
        )
    )
);

