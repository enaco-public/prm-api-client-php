<?php

namespace LIG\PRM\API;

use Guzzle\Common\Collection;
use Guzzle\Http\Exception\BadResponseException;
use Guzzle\Http\Message\Response;
use Guzzle\Service\Command\CommandInterface;
use Guzzle\Service\Description\ServiceDescription;

class Client extends \Guzzle\Service\Client
{
    /**
     * @param array $config
     *
     * @return \Guzzle\Service\Client|Client
     */
    public static function factory($config = array())
    {
        $default = array(
            'redirect.disable' => false,
            'curl.options' => array(
                'CURLOPT_TIMEOUT' => 10
            ),
            'locale' => 'FR'
        );

        $required = array('base_url', 'api_key', 'locale');

        $config = Collection::fromConfig($config, $default, $required);

        $config['request.options'] = array(
            'query' => array('api_key' => $config['api_key'])
        );

        $client = new self($config->get('base_url'), $config);

//        $client->addSubscriber(LogPlugin::getDebugPlugin());

        $client->setDescription(ServiceDescription::factory(realpath(__DIR__ . '/Resources/prm-api-description.php')));

        return $client;
    }

    /**
     * @return array(Country)|void
     */
    public function getCountries($isoCode = 'fr', $limit = 300)
    {
        $command = $this->getCommand('getCountries', array('limit' => (int)$limit));

        //$res = $this->executeCommand($command);
        $response = $command->getResponse();
        $countries = $response->json();

        $finalObjects = array();
        
        foreach ($countries as $country) {
            $name = null;

            foreach ($country['langs'] as $locale) {
                if($locale['lang']['iso_code'] == $isoCode)
                    $name = $locale['name'];
            }

            $finalObjects[] = new \LIG\PRM\API\Model\Country($country['id'], $country['iso_code'], $country['phone_code'], $name);
        }

        return count($finalObjects) ? $finalObjects : null;
    }

    /**
     * @return array(LevelsStudy)|void
     */
    public function getLevelsStudy($isoCode = 'fr', $limit = 300)
    {
        $command = $this->getCommand('getLevelsStudy', array('limit' => (int)$limit));

        //$res = $this->executeCommand($command);
        $response = $command->getResponse();
        $levelsStudy = $response->json();

        $finalObjects = array();
        
        foreach ($levelsStudy as $levelStudy) {
            $name = null;

            foreach ($levelStudy['langs'] as $locale) {
                if($locale['lang']['iso_code'] == $isoCode)
                    $name = $locale['name'];
            }

            $finalObjects[] = new \LIG\PRM\API\Model\LevelStudy($levelStudy['id'], $name);
        }

        return count($finalObjects) ? $finalObjects : null;
    }

    /**
     * @param string $isoCode
     *
     * @return Response|void
     */
    public function getFormations($isoCode = 'fr')
    {
        $command = $this->getCommand('getFormations', array('lang' => $isoCode));

        return $this->executeCommand($command);
    }

    public function getGenders()
    {
        $command = $this->getCommand('getGenders');

        return $this->executeCommand($command);
    }
	
    /**
     * Retourne les situations d'un futur prospect
     */
    public function getSituations($isoCode = 'fr'){
    	$command = $this->getCommand('getSituations', array('lang' => $isoCode));
    	
    	return $this->executeCommand($command);
    }
    
    /**
     * @param $email
     * @param $formation
     * @param null $firstname
     * @param null $lastname
     * @param null $idGender
     * @param null $birthday
     * @param null $phone
     * @param null $address1
     * @param null $address2
     * @param null $postcode
     * @param null $city
     * @param null $country
     * @param null $type
     *
     * @return Response|void
     */
    public function addCoupon(
        $email,
        $formation,
        $firstname = null,
        $lastname = null,
        $idGender = null,
        $birthday = null,
        $phone = null,
        $address1 = null,
        $address2 = null,
        $postcode = null,
        $city = null,
        $country = null,
        $type = null,
    	$accepteVisio = false
    ) {

        $args = array(
            'email' => $email,
            'formation' => $formation,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'idGender' => $idGender,
            'birthday' => $birthday,
            'phone' => $phone,
            'address1' => $address1,
            'address2' => $address2,
            'postcode' => $postcode,
            'city' => $city,
            'country' => $country,
            'type' => $type,
        	'accepteVisio' => $accepteVisio
        );

        $command = $this->getCommand('addCoupon', $args);
        return $this->executeCommand($command);
    }

    /**
     * @param $phone
     *
     * @return Response|void
     */
    public function getProspectByPhone($phone)
    {
        $command = $this->getCommand('getProspectByPhone', array('phone' => $phone));

        return $this->executeCommand($command);
    }


    /**
     * @return Response|void
     */
    public function newProspect()
    {
        $command = $this->getCommand('newProspect');

        return $this->executeCommand($command);
    }

    /**
     * @param $email
     *
     * @return Response|void
     */
    public function getProspectByEmail($email)
    {
        $command = $this->getCommand('getProspectByEmail', array('criteria[email]' => $email));

        return $this->executeCommand($command);
    }

    /**
     * @param $id
     *
     * @return Response|void
     */
    public function getProspect($id)
    {
        $command = $this->getCommand('getProspect', array('id' => $id));

        return $this->executeCommand($command);
    }

    /**
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $idGender
     * @param $birthday
     * @param $country
     *
     * @return Response|void
     */
    public function addProspect(
        $email,
        $firstname = null,
        $lastname = null,
        $idGender = null,
        $birthday = null,
        $country = null
    ) {

        $args = array(
            'customer[email]' => $email,
            'customer[firstname]' => $firstname,
            'customer[lastname]' => $lastname,
            'customer[idGender]' => $idGender,
            'customer[birthday]' => $birthday,
            'customer[country]' => $country,
        );

        $command = $this->getCommand('addProspect', $args);

        return $this->executeCommand($command);
    }

    /**
     * @param $id
     * @param $email
     * @param $firstname
     * @param $lastname
     * @param $idGender
     * @param $birthday
     * @param $country
     *
     * @return Response|void
     */
    public function patchProspect(
        $id,
        $email = null,
        $firstname = null,
        $lastname = null,
        $idGender = null,
        $birthday = null,
        $country = null
    ) {
        $args = array(
            'id' => $id,
            'customer[email]' => $email,
            'customer[firstname]' => $firstname,
            'customer[lastname]' => $lastname,
            'customer[idGender]' => $idGender,
            'customer[birthday]' => $birthday,
            'customer[country]' => $country
        );

        $command = $this->getCommand('patchProspect', $args);

        return $this->executeCommand($command);
    }

    /**
     * @param $idProspect
     * @param $formation
     * @param null $type
     *
     * @return Response|void
     */
    public function addProspectCoupon($idProspect, $formation = null, $type = null, $accepteVisio = false)
    {
        $args = array(
            'coupon[customer]' => $idProspect,
            'coupon[formation]' => $formation,
            'coupon[type]' => $type,
        	'coupon[accepteVisio]' => $accepteVisio
        );

        $command = $this->getCommand('addProspectCoupon', $args);

        return $this->executeCommand($command);
    }

    /**
     * @param $idProspect
     * @param $address1
     * @param $address2
     * @param $postcode
     * @param $city
     * @param $phone
     * @param $idCountry
     *
     * @return Response|void
     */
    public function addAddress(
        $idProspect,
        $address1 = null,
        $address2 = null,
        $postcode = null,
        $city = null,
        $phone = null,
        $idCountry = null
    ) {
        $args = array(
            'address[customer]' => $idProspect,
            'address[address1]' => $address1,
            'address[address2]' => $address2,
            'address[postcode]' => $postcode,
            'address[city]' => $city,
            'address[phone]' => $phone,
            'address[idCountry]' => $idCountry,
        );

        $command = $this->getCommand('addAddress', $args);

        return $this->executeCommand($command);
    }

    /**
     * @param $id
     * @param $idProspect
     * @param $address1
     * @param $address2
     * @param $postcode
     * @param $city
     * @param $phone
     *
     * @return Response|void
     */
    public function patchAddress(
        $id,
        $idProspect,
        $address1 = null,
        $address2 = null,
        $postcode = null,
        $city = null,
        $phone = null,
        $idCountry = null
    ) {
        $args = array(
            'id' => $id,
            'address[customer]' => $idProspect,
            'address[address1]' => $address1,
            'address[address2]' => $address2,
            'address[postcode]' => $postcode,
            'address[city]' => $city,
            'address[phone]' => $phone,
            'address[idCountry]' => $idCountry,
        );

        $command = $this->getCommand('patchAddress', $args);

        return $this->executeCommand($command);
    }

    /**
     * @param $phone
     *
     * @return Response|void
     */
    public function getAddressByPhone($phone)
    {
        $command = $this->getCommand('getAddressByPhone', array('criteria[phone]' => $phone));

        return $this->executeCommand($command);
    }

    /**
     * @param $phone
     *
     * @return Response|void
     */
    public function getAddressByPhoneMobile($phone)
    {
        $command = $this->getCommand('getAddressByPhoneMobile', array('criteria[phoneMobile]' => $phone));

        return $this->executeCommand($command);
    }

    /**
     * @param $prospectId
     *
     * @return Response|void
     */
    public function getAddressByProspect($prospectId)
    {
        $command = $this->getCommand('getAddressByProspect', array('criteria[customer]' => $prospectId));

        return $this->executeCommand($command);
    }

    /**
     * @param CommandInterface $command
     *
     * @return \Guzzle\Http\Message\Response|void
     */
    public function executeCommand(CommandInterface $command)
    {
        try {
            $response = $this->send($this->prepareCommand($command));
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }

        return $response;
    }

    /**
     * @param string $name
     * @param array $args
     *
     * @return CommandInterface|null
     */
    public function getCommand($name, array $args = array())
    {
        $args = $this->normalizeArgs($args);

        return parent::getCommand($name, $args);
    }
    /**
     * @param Response $response
     *
     * @return array
     */
    public function getErrors(Response $response)
    {
        return array('client' => $this->getClientErrors($response), 'server' => $this->getServerErrors($response));
    }

    /**
     * @param Response $response
     *
     * @return array
     */
    public function getClientErrors(Response $response)
    {
        $clientErrors = array();

        if ($response->isClientError()) {

            if ($response->getBody()->getSize() != 0) {
                $body = $response->json();
                foreach ($body['errors']['children'] as $field => $fieldErrors) {
                    if (array_key_exists('errors', $fieldErrors)) {
                        $clientErrors[$field] = array();
                        foreach ($fieldErrors['errors'] as $fieldError) {
                            $clientErrors[$field][] = $fieldError;
                        }
                    }
                }
            }
        }

        return $clientErrors;
    }

    /**
     * @param Response $response
     *
     * @return array
     */
    public function getServerErrors(Response $response)
    {
        $serverErrors = array();

        if ($response->isServerError()) {
            $serverErrors['status_code'] = $response->getStatusCode();
            $serverErrors['reason_phrase'] = $response->getReasonPhrase();
            $serverErrors['content'] = $response->json();
        }

        return $serverErrors;
    }

    private function normalizeArgs(array $args = array())
    {
        array_walk($args, function ($value, $key) use (&$args) {
            if (is_null($value)) {
                unset($args[$key]);
            }
        });

        return $args;
    }
}