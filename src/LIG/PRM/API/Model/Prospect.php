<?php

namespace LIG\PRM\API\Model;

use Guzzle\Service\Command\OperationCommand;

class Prospect extends Response
{
    protected $email;

    protected $firstname;

    protected $lastname;

    protected $gender;

    protected $birthday;

    protected $country;

    public function __construct($email, $firstname, $lastname, $gender, $country)
    {
        $this->email = $email;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->gender = $gender;
        $this->country = $country;
    }

    public static function fromCommand(OperationCommand $command)
    {
        parent::fromCommand($command);

        $response = $command->getResponse();

        $json = $response->json();
		//var_dump($json);
        return new self($json['email'], $json['firstname'], $json['lastname'], $json['id_gender'], $json['country']);
    }
}