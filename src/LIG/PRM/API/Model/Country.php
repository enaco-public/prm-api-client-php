<?php

namespace LIG\PRM\API\Model;

use Guzzle\Service\Command\OperationCommand;
use Guzzle\Service\Command\ResponseClassInterface;

class Country implements ResponseClassInterface
{
    protected $id;

    protected $iso_code;

    protected $phone_code;

    protected $name;

    public function __construct($id, $iso_code, $phone_code, $name)
    {
        $this->id = $id;
        $this->iso_code = $iso_code;
        $this->phone_code = $phone_code;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIso_code()
    {
        return $this->iso_code;
    }

    public function getPhone_code()
    {
        return $this->phone_code;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function fromCommand(OperationCommand $command)
    {
        $response = $command->getResponse();

        $json = $response->json();

        return new self($json['id'], $json['iso_code'], $json['phone_code'], $name);
    }
}