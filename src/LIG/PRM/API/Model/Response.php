<?php

namespace LIG\PRM\API\Model;

use Guzzle\Service\Command\OperationCommand;
use Guzzle\Service\Command\ResponseClassInterface;

class Response implements ResponseClassInterface
{
    protected static $errors = array();

    public static function fromCommand(OperationCommand $command)
    {
        $response = $command->getResponse();

        if ($response->isClientError()) {
            $clientErrors = $response->json();
            self::$errors = $clientErrors['children'];
        }
    }
}