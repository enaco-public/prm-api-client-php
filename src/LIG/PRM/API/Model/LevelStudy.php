<?php

namespace LIG\PRM\API\Model;

use Guzzle\Service\Command\OperationCommand;
use Guzzle\Service\Command\ResponseClassInterface;

class LevelStudy implements ResponseClassInterface
{
    protected $id;

    protected $name;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function fromCommand(OperationCommand $command)
    {
        $response = $command->getResponse();

        $json = $response->json();

        return new self($json['id'], $json['iso_code'], $json['phone_code'], $name);
    }
}