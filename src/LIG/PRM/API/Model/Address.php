<?php

namespace LIG\PRM\API\Model;

use Guzzle\Service\Command\OperationCommand;
use Guzzle\Service\Command\ResponseClassInterface;

class Address implements ResponseClassInterface
{
    protected $address1;

    protected $address2;

    protected $postcode;

    protected $city;

    protected $phone;

    protected $country;

    protected $prospect;

    public function __construct($address1, $address2, $postcode, $city, $phone, $country, $prospect)
    {
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->country = $country;
        $this->prospect = $prospect;
    }

    public static function fromCommand(OperationCommand $command)
    {
        $response = $command->getResponse();

        $json = $response->json();

        return new self($json['address1'], $json['address2'], $json['postcode'], $json['city'], $json['phone'], $json['country'],
            $json['prospect']);
    }
}