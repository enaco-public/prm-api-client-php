<?php

namespace LIG\PRM\API\Command;

use Guzzle\Service\Command\AbstractCommand;

class GetProspectByPhoneCommand extends AbstractCommand
{
    protected function build()
    {

        $phone = $this->get('phone');

        $command = $this->getClient()->getCommand('getAddressByPhone', array(
            'criteria[phone]' => $phone
        ));

        /** @var \Guzzle\Http\Message\Response $response */
        $response = $this->getClient()->executeCommand($command);

        if (!$response->isError()) {
            // prospect exists by phone
            if ($response->getStatusCode() == 200) {
                // get prospect by address
                $response = $response->json();
                $links = $response['_links'];

                //@todo: fix api prefix
                $this->request = $this->getClient()->createRequest('GET', $this->getClient()->getBaseUrl() . $links['customer']['href']);
            } else {
                // prospect not exists by
                $command = $this->getClient()->getCommand('getAddressByPhoneMobile', array(
                    'criteria[phoneMobile]' => $phone
                ));
                $response = $this->getClient()->executeCommand($command);

                if (!$response->isError()) {
                    // prospect exists by phoneMobile
                    if ($response->getStatusCode() == 200) {
                        // get prospect by address
                        $response = $response->json();
                        $links = $response['_links'];

                        //@todo: fix api prefix
                        $this->request = $this->getClient()->createRequest('GET', $this->getClient()->getBaseUrl() . $links['customer']['href']);
                    } else {
                        // generate request who generate no content
                        $this->request = $this->getClient()->createRequest('GET',$this->getClient()->getBaseUrl() . '/customers/show?criteria[id]=0');
                    }
                } else {
                    $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                }
            }
        }else {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
        }
    }
}