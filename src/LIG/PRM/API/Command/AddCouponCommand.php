<?php

namespace LIG\PRM\API\Command;

use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Service\Command\AbstractCommand;

class AddCouponCommand extends AbstractCommand
{
    /**
     * @var \LIG\PRM\API\Client $client
     */
    protected $client;

    protected function build()
    {
        $email = $this->get('email');
        $firstname = $this->get('firstname');
        $lastname = $this->get('lastname');
        $idGender = $this->get('idGender');
        $birthday = $this->get('birthday');
        $address1 = $this->get('address1');
        $address2 = $this->get('address2');
        $postcode = $this->get('postcode');
        $city = $this->get('city');
        $country = $this->get('country');
        $phone = $this->get('phone');
        $type = $this->get('type');
        $formation = $this->get('formation');
        $accepteVisio = $this->get('accepteVisio');

        if (!($prospect = $this->getProspectByEmail($email))) {
            return;
        }

        // prospect exists by email
        if ($prospect->getStatusCode() == 200 && !is_null($email)) {
            $prospect = $prospect->json();

            // patch prospect
            if (!$this->patchProspect($prospect['id'], $email, $firstname, $lastname, $idGender, $birthday,
                $country)
            ) {
                return;
            }

            if (!($address = $this->getAddressByProspect($prospect['id']))) {
                return;
            }

            if ($address->getStatusCode() == 200) {
                $address = $address->json();

                //@todo: patch address, need get idAddress from prospect
                if (!$this->patchAddress($address['id'], $prospect['id'], $address1, $address2, $postcode, $city, $phone,
                    $country)
                ) {
                    return;
                }
            }else {
                // add adress to existing prospect
                $command = $this->client->getCommand('addAddress', array(
                    'address[customer]' => $prospect['id'],
                    'address[address1]' => $address1,
                    'address[address2]' => $address2,
                    'address[postcode]' => $postcode,
                    'address[city]' => $city,
                    'address[phone]' => $phone,
                    'address[idCountry]' => $country
                ));

                $newAddress = $this->client->executeCommand($command);

                if ($newAddress->isError()) {
                    $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                    return ;
                }
            }

            // add coupon to existing prospect
            $command = $this->client->getCommand('addProspectCoupon', array(
                'coupon[type]' => $type,
                'coupon[formation]' => $formation,
                'coupon[customer]' => $prospect['id'],
         		'coupon[accepterVisio]' => $accepteVisio
            ));

            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
        } else {
            // prospect not exists by email
            if (!($prospect = $this->getProspectByPhone($phone))) {
                return;
            }

            // prospect exists by phone or mobilePhone
            if ($prospect->getStatusCode() == 200 && !is_null($phone)) {
                $prospect = $prospect->json();

                // patch prospect
                if (!$this->patchProspect($prospect['id'], $email, $firstname, $lastname, $idGender, $birthday,
                    $country)
                ) {
                    return;
                }


                if (!($address = $this->getAddressByProspect($prospect['id']))) {
                    return;
                }

                if ($address->getStatusCode() == 200) {
                    $address = $address->json();

                    //@todo: patch address, need get idAddress from prospect
                    if (!$this->patchAddress($address['id'], $prospect['id'], $address1, $address2, $postcode, $city, $phone,
                        $country)
                    ) {
                        return;
                    }
                }else {
                    // add adress to existing prospect
                    $command = $this->client->getCommand('addAddress', array(
                        'address[customer]' => $prospect['id'],
                        'address[address1]' => $address1,
                        'address[address2]' => $address2,
                        'address[postcode]' => $postcode,
                        'address[city]' => $city,
                        'address[phone]' => $phone,
                        'address[idCountry]' => $country
                    ));

                    $newAddress = $this->client->executeCommand($command);

                    if ($newAddress->isError()) {
                        $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                        return ;
                    }
                }


                // add coupon to existing prospect
                $command = $this->client->getCommand('addProspectCoupon', array(
                    'coupon[type]' => $type,
                    'coupon[formation]' => $formation,
                    'coupon[customer]' => $prospect['id'],
             		'coupon[accepterVisio]' => $accepteVisio
                ));

                $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
            } else {
                // prospect not exists

                // add new prospect with address and coupon
                $args = array(
                    'customer[email]' => $email,
                    'customer[firstname]' => $firstname,
                    'customer[lastname]' => $lastname,
                    'customer[idGender]' => $idGender,
                    'customer[birthday]' => $birthday,
                    'customer[country]' => $country,
                );
                $command = $this->client->getCommand('addProspect', $args);

                $newProspect = $this->client->executeCommand($command);

                if (!$newProspect->isError()) {
                    if ($newProspect->getStatusCode() == 201) {
                        $newProspect = $newProspect->json();

                        $command = $this->client->getCommand('addAddress', array(
                            'address[customer]' => $newProspect['id'],
                            'address[address1]' => $address1,
                            'address[address2]' => $address2,
                            'address[postcode]' => $postcode,
                            'address[city]' => $city,
                            'address[phone]' => $phone,
                            'address[idCountry]' => $country
                        ));

                        $newAddress = $this->client->executeCommand($command);

                        if (!$newAddress->isError()) {
                            if ($newAddress->getStatusCode() == 201) {
                                $command = $this->client->getCommand('addProspectCoupon', array(
                                    'coupon[type]' => $type,
                                    'coupon[formation]' => $formation,
                                    'coupon[customer]' => $newProspect['id'],
                                	'coupon[accepterVisio]' => $accepteVisio
                                ));

                                $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                            }
                        } else {
                            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                        }
                    }
                } else {
                    $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());
                }

            }

        }
    }

    private function getProspectByEmail($email)
    {
        $command = $this->client->getCommand('getProspectByEmail', array('criteria[email]' => $email));
        /** @var \Guzzle\Http\Message\Response $response */
        $response = $this->client->executeCommand($command);

        if ($response->isError()) {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());

            return false;
        }

        return $response;
    }

    private function getProspectByPhone($phone)
    {
        $command = $this->client->getCommand('getProspectByPhone', array('phone' => $phone));
        /** @var \Guzzle\Http\Message\Response $response */
        $response = $this->client->executeCommand($command);

        if ($response->isError()) {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());

            return false;
        }

        return $response;
    }

    private function patchProspect(
        $id,
        $email = null,
        $firstname = null,
        $lastname = null,
        $idGender = null,
        $birthday = null,
        $country = null
    ) {
        $args = array(
            'id' => $id,
            'customer[email]' => $email,
            'customer[firstname]' => $firstname,
            'customer[lastname]' => $lastname,
            'customer[idGender]' => $idGender,
            'customer[birthday]' => $birthday,
            'customer[country]' => $country
        );

        $command = $this->client->getCommand('patchProspect', $args);

        $patchProspect = $this->client->executeCommand($command);

        if ($patchProspect->isError()) {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());

            return false;
        }

        return true;
    }

    private function getAddressByProspect($prospectId)
    {
        $command = $this->client->getCommand('getAddressByProspect', array('criteria[customer]' => $prospectId));
        /** @var \Guzzle\Http\Message\Response $response */
        $response = $this->client->executeCommand($command);

        if ($response->isError()) {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());

            return false;
        }

        return $response;
    }

    private function patchAddress(
        $id,
        $idProspect,
        $address1 = null,
        $address2 = null,
        $postcode = null,
        $city = null,
        $phone = null,
        $idCountry = null
    ) {
        $args = array(
            'id' => $id,
            'address[customer]' => $idProspect,
            'address[address1]' => $address1,
            'address[address2]' => $address2,
            'address[postcode]' => $postcode,
            'address[city]' => $city,
            'address[phone]' => $phone,
            'address[idCountry]' => $idCountry,
        );

        $command = $this->client->getCommand('patchAddress', $args);

        $patchAddress = $this->client->executeCommand($command);

        if ($patchAddress->isError()) {
            $this->request = ($command->isPrepared() ? $command->getRequest() : $command->prepare());

            return false;
        }

        return true;
    }
}