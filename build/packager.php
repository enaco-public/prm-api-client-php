<?php
require __DIR__ . '/Burgomaster.php';

// Creating staging directory at guzzlehttp/src/build/artifacts/staging.
$stageDirectory = __DIR__ . '/staging';
// The root of the project is up one directory from the current directory.
$projectRoot = __DIR__ . '/../';
$packager = new \Burgomaster($stageDirectory, $projectRoot);
$autoloadFilename = 'prm-api-autoloader.php';

// Copy basic files to the stage directory. Note that we have chdir'd onto
// the $projectRoot directory, so use relative paths.
$metaFiles = array('readme.md');
foreach ($metaFiles as $file) {
$packager->deepCopy($file, $file);
}

// Copy each dependency to the staging directory. Copy *.php and *.pem files.
$packager->recursiveCopy('src/LIG', 'LIG', array('php', 'json'));
$packager->recursiveCopy('vendor/doctrine/instantiator/src/Doctrine', 'Doctrine/Instantiator');
$packager->recursiveCopy('vendor/guzzle/guzzle/src/Guzzle', 'Guzzle', array('php', 'pem'));
$packager->recursiveCopy('vendor/symfony/event-dispatcher/Symfony', 'Symfony');
$packager->recursiveCopy('vendor/symfony/yaml/Symfony', 'Symfony');


// Create the classmap autoloader, and instruct the autoloader to
// automatically require the 'GuzzleHttp/functions.php' script.
$packager->createAutoloader(array(), $autoloadFilename);
// Create a zip file from the staging directory at a specific location
$packager->createZip(__DIR__ . '/prm-api-client-php.zip');
